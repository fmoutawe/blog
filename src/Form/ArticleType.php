<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextType::class, array('attr' => array(
                'placeholder' => 'Title'
            )))
            ->add('url', FileType::class, array("data_class" => null))
            ->add('content', CKEditorType::class, array('label' => false, 'config' => ['toolbar' => 'full', 'height' => 300]))
            ->add('tag', TextType::class, array('label' => false, 'attr' => array(
                'placeholder' => 'Tag'
            )))
            ->add('submit', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}
