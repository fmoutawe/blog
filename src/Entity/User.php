<?php

namespace App\Entity;

class User {
  public $id;
  public $name;
  private $password;
  public $email;

  public function _construct(int $id, string $name, string $password, string $email) {

    $this->id = $id;
    $this->name = $name;
    $this->password = $password;
    $this->email = $email;
  }
  public static function fromSQL(array $rawData) {
    return new User(
        $rawData["id"],
        $rawData["name"],
        $rawData["password"],
        $rawData["email"]
        
    );
}

}