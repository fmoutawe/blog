<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;


class Home extends AbstractController{
  /**
   * @Route("/home", name="home")
   */
  public function index(ArticleRepository $repo){
    
    $listArticle = $repo->getAll();

    dump ($listArticle);
    
    return $this->render("home.html.twig", ["index"=>$listArticle]);
  }
}