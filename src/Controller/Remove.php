<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use App\Form\ArticleType;

class Remove extends Controller
{
  /**
     * @Route("/admin/remove/{id}", name="remove_article")
     */
    public function remove(int $id, ArticleRepository $repo) 
    {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }
}